const get = async (req, res) => {
    try {
        return res.status(200).json({
            message : 'Succes Get Data',
            data : {
                name : 'name product'
            }
        })
    } catch (error) {
        return res.status(401).json({
            message : 'Failed Get Data',
            data : error
        })
    }
    
}

const getById = async (req, res) => {
    let {id} = req.params
    try {
        return res.status(200).json({
            message : `Succes Get Data ${id}`,
            data : {
                name : 'new name product'
            }
        })
    } catch (error) {
        return res.status(401).json({
            message : `Failed Get Data ${id}`,
            data : error
        })
    }
    
}

const post = async (req, res) => {
    let {name, description} = req.body
    try {
        return res.status(200).json({
            message : `Succes Create Data`,
            data : {
                name,
                description,
            }
        })
    } catch (error) {
        return res.status(401).json({
            message : `Failed Create Data `,
            data : error
        })
    }
    
}

const update = async (req, res) => {
    let {id} = req.params
    let {name ,description} = req.body
    try {
        return res.status(200).json({
            message : `Succes Update Data ${id}`,
            data : {
                name,
                description,
            }
        })
    } catch (error) {
        return res.status(401).json({
            message : `Failed Update Data ${id}`,
            data : error
        })
    }
    
}

const deletes = async (req, res) => {
    let {id} = req.params
    try {
        return res.status(200).json({
            message : `Succes delete Data ${id}`,
            data : {
                name : 'name product'
            }
        })
    } catch (error) {
        return res.status(401).json({
            message : `Failed delete Data`,
            data : error
        })
    }
    
}


module.exports = {
    get, getById, post, update, deletes
}