const options = {
    definition: {
         openapi: '3.0.0',
         info: {
             title: 'Swagger Test',
             version: '1.0.0',
             description: 'This is simple CRUD API with Express and documented with Swagger'
         },
         servers: [
           {
             url: "http://localhost:8080",
             description: 'Development server'
           },
         ],
         components: {
            schemas: {
                Product: {
                    type: 'object',
                    required: ['name'],
                    properties: {
                        name: {
                              type: 'string',
                              description : 'The name of product'
                        },
                        description: {
                            type: 'string',
                            description : 'The description of product'
                        },
                        category_id: {
                          type: 'array',
                          description: 'The category id of product',
                          items: {
                              type : 'interger'
                          }
                        },
                    },
                    example: {
                      name : "Product 1",
                      description : "Descripsi 1",
                    }
                  },
            },
             responses: {
                 400: {
                     description: 'Missing API key - include it in the Authorization header',
                     contents: 'application/json'
                 },
                 401: {  
                     description: 'Unauthorized - incorect API key or inccorect format',
                     contents: 'application/json'
                 },
                 404: {
                     description: 'Not found - the article was not found',
                     contents: 'application/json'
                 }
             },
             securitySchemes:{
                 bearerAuth:{
                     type: 'http',
                     // in: 'header',
                     scheme: 'bearer',
                     bearerFormat: 'JWT',
                     name: 'Authorization'
                 }
             }
         },
         // security:[{
         //     bearerAuth: []
         // }]
     },
     apis: ['./router/*', ] 
 }
 
 module.exports = options 