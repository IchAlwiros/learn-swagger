const express = require("express")
const {get, getById, post, update, deletes} = require("../controllers/productControllers")

const productRouter = express.Router()
/**
 * @swagger
 * tags:
 *   name: Product
 *   description: API endpoints to manage Product
 */

/**
 * @swagger
 *   /product:
 *        get:
 *         description: Get All Product 
 *         summary: Get a product
 *         tags: [Product]
 *         responses:
 *           "200":
 *             description: The list of Product
 *             contents:
 *               aplication/json:
 *                   schema:
 *                       $ref: '#/components/schemas/Product'
 *           "400":
 *             $ref: '#/components/responses/400'
 *           "401":
 *             $ref: '#/components/responses/401'
 */

productRouter.get("/product", get)

/**
 * @swagger
 *   /product/{id}:
 *     get:
 *       summary: Get a Product by id
 *       tags: [Product]
 *       parameters:
 *         - in: path
 *           name: id
 *           schema:
 *             type: string
 *           required: true
 *           description: Id of a Product
 *       responses:
 *         "200":
 *           description: The Product
 *           contents:
 *             application/json:
 *               schema:
 *                 $ref: '#/components/schemas/Product'
 *         "400":
 *           $ref: '#/components/responses/400'
 *         "401":
 *           $ref: '#/components/responses/401'
 *         "404":
 *           $ref: '#/components/responses/404'
 */
productRouter.get("/product/:id", getById)

/** 
 * @swagger
 *   /product:
 *     post:     
 *       security:
 *          -   bearerAuth: []     
 *       summary: Create a product <Baerer Token Required>
 *       tags: [Product]
 *       requestBody:
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Product'
 *       responses:
 *         400:
 *           $ref: '#/components/responses/400'
 *         401:
 *           $ref: '#/components/responses/401'
 *         200:
 *           description: Product created successfully
 *           contents:
 *             application/json
 */
productRouter.post("/product", post)


/** 
 * @swagger
 *   /product/{id}:
 *     put:
 *       security:
 *          -   bearerAuth: []     
 *       summary: Updated a product <Baerer Token Required>
 *       tags: [Product]
 *       parameters:
 *         - in: path
 *           name: id
 *           schema:
 *             type: string
 *           required: true
 *           description: Id of a product
 *       requestBody:
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Product'
 *               required:
 *       responses:
 *          400:
 *           $ref: '#/components/responses/400'
 *          401:
 *           $ref: '#/components/responses/401'
 *          200:
 *           description: Product updated successfully
 *           contents:
 *             application/json
 */
productRouter.put("/product/:id", update)


/** 
 * @swagger
 *   /product/{id}:
 *     delete:
 *       security:
 *          -   bearerAuth: []     
 *       summary: Deleted a Product <Baerer Token Required>
 *       tags: [Product]
 *       parameters:
 *         - in: path
 *           name: id
 *           schema:
 *             type: string
 *           required: true
 *           description: Id of a Product
 *       responses:
 *         400:
 *           $ref: '#/components/responses/400'
 *         401:
 *           $ref: '#/components/responses/401'
 *         204:
 *           description: Product deleted successfully
 *           contents:
 *             application/json
 */
productRouter.delete("/product/:id", deletes)

module.exports = productRouter