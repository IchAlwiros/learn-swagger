const express = require('express')

const swaggerJSDoc = require('swagger-jsdoc');
const swagerUi = require('swagger-ui-express')
const  options  = require('./doc/jsDoc');


const app = express()
const port = 8080


app.use(express.json())


// Router
const {Product} = require('./router/index')
app.use("/",  Product,)


// Swagger Setup
var optionsSwaggerCss = {
    customCssUrl: 'https://unpkg.com/swagger-ui-dist@4.5.0/swagger-ui.css'
  };


//   Initial Swagger
const specs = swaggerJSDoc(options)
app.use('/api-docs', swagerUi.serve, swagerUi.setup(specs,optionsSwaggerCss))


app.listen(port, () =>{
    console.log(`app on port ${port}`);
})